package ru.tsc.bagrintsev.tm.exception.field;

public final class IncorrectStatusException extends AbstractFieldException {

    public IncorrectStatusException() {
        super("Error! Status is incorrect...");
    }

}
