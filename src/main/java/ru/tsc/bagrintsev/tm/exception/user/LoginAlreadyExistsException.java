package ru.tsc.bagrintsev.tm.exception.user;

import org.jetbrains.annotations.NotNull;

public final class LoginAlreadyExistsException extends AbstractUserException {

    public LoginAlreadyExistsException(@NotNull final String login) {
        super(String.format("Error! Login %s is already in use...", login));
    }

}
