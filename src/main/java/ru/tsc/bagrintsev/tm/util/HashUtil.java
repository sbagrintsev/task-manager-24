package ru.tsc.bagrintsev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

public interface HashUtil {

    int KEY_LENGTH = 128;

    int ITERATION_COUNT = 65536;

    @NotNull
    static byte[] generateSalt() {
        @NotNull SecureRandom random = new SecureRandom();
        @NotNull byte[] salt = new byte[16];
        random.nextBytes(salt);
        return salt;
    }

    @NotNull
    static String generateHash(@NotNull String password, @Nullable byte[] salt) throws GeneralSecurityException {
        @NotNull KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH);
        @NotNull SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        @NotNull byte[] hash = factory.generateSecret(spec).getEncoded();
        return new String(hash, StandardCharsets.UTF_8);
    }

}
