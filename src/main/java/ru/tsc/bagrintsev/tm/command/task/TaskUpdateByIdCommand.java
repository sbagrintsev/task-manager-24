package ru.tsc.bagrintsev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("ENTER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NAME: ");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getTaskService().updateById(userId, id, name, description);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-update-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update task by id.";
    }
}
