package ru.tsc.bagrintsev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class TaskBindToProjectCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws AbstractException, IOException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.print("ENTER TASK ID: ");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-bind-to-project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Bind task to project.";
    }
}
