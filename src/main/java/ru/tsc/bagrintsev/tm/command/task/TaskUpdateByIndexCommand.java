package ru.tsc.bagrintsev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getTaskService().updateByIndex(userId, index, name, description);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-update-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update task by index.";
    }
}
