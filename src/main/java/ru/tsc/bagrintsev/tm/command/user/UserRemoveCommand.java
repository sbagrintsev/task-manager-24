package ru.tsc.bagrintsev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public final class UserRemoveCommand extends AbstractUserCommand {

    @Override
    public void execute() throws IOException, AbstractException {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Admin functional: remove user.";
    }

}
