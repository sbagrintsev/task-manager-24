package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class VersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("task-manager version 1.23.0");
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print version.";
    }

}
