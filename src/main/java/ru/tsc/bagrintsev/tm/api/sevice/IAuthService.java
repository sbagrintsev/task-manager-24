package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;

public interface IAuthService extends Checkable {

    void signIn(
            @Nullable final String login,
            @Nullable final String password) throws AbstractException, GeneralSecurityException;

    void signOut();

    @Nullable
    String getCurrentUserId();

    @NotNull
    User getCurrentUser() throws AbstractException;

    boolean isAuth();

    void checkRoles(@NotNull final Role[] roles) throws AbstractException;

}
